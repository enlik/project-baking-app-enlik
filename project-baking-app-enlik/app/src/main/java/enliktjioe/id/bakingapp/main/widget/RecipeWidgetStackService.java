package enliktjioe.id.bakingapp.main.widget;

import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import java.util.Locale;

import javax.inject.Inject;

import enliktjioe.id.bakingapp.R;
import enliktjioe.id.bakingapp.data.RecipesCache;
import enliktjioe.id.bakingapp.data.model.IngredientsItem;

import static enliktjioe.id.bakingapp.BaseActivity.getGraph;


public class RecipeWidgetStackService extends RemoteViewsService {

    @Inject RecipesCache recipesCaches;

    @Override
    public void onCreate() {
        super.onCreate();

        getGraph().inject(this);
    }


    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        return new StackRemoteViewsFactory(getApplicationContext());
    }


    class StackRemoteViewsFactory implements
            RemoteViewsService.RemoteViewsFactory {

        Context mContext;

        public StackRemoteViewsFactory(Context context) {
            mContext = context;
        }


        public void onCreate() {
        }

        @Override
        public void onDataSetChanged() {

        }

        @Override
        public void onDestroy() {

        }

        @Override
        public int getCount() {
            return recipesCaches.size();
        }

        @Override
        public RemoteViews getViewAt(int i) {
            RemoteViews rv = new RemoteViews(mContext.getPackageName(), R.layout.widget_nutella_pie);
            rv.setTextViewText(R.id.widget_title_nutella_pie, recipesCaches.get(i).name());

            StringBuilder sb = new StringBuilder();
            for (IngredientsItem ingredientsItem : recipesCaches.get(i).ingredients()) {
                if (sb.length() != 0) {
                    sb.append("\n");
                }

                sb.append(String.format(Locale.US, "%.1f %s %s", ingredientsItem.quantity(), ingredientsItem.measure(), ingredientsItem.ingredient()));
            }

            rv.setTextViewText(R.id.widget_content_nutella_pie, sb.toString());
            return rv;
        }

        @Override
        public RemoteViews getLoadingView() {
            return null;
        }

        @Override
        public int getViewTypeCount() {
            return 1;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }
    }
}
