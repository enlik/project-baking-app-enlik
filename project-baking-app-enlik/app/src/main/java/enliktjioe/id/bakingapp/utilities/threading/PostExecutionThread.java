package enliktjioe.id.bakingapp.utilities.threading;


import io.reactivex.Scheduler;

public interface PostExecutionThread {
    Scheduler getScheduler();
}