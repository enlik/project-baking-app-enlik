package enliktjioe.id.bakingapp.main.recipe.summary.items;

import android.view.View;

import enliktjioe.id.bakingapp.main.view.ViewHolder;


public abstract class RecipeSummaryItemViewHolder<T extends View> extends ViewHolder<T> {


    public RecipeSummaryItemViewHolder(T itemView) {
        super(itemView);
    }
}
