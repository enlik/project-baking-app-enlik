package enliktjioe.id.bakingapp.data;

import com.google.gson.Gson;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import enliktjioe.id.bakingapp.BuildConfig;
import enliktjioe.id.bakingapp.data.model.Recipe;
import enliktjioe.id.bakingapp.data.repo.recipes.RecipesRepository;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

public class RetrofitRecipesRepository implements RecipesRepository {

    private final RecipesService service;

    interface RecipesService {
        @GET(BuildConfig.UDACITY_ANDROID_BAKING_JSON_URL)
        Single<List<Recipe>> getAll();

    }

    @Inject
    public RetrofitRecipesRepository(Gson gson) {
        Retrofit retrofit = new Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(BuildConfig.UDACITY_GO_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        service = retrofit.create(RecipesService.class);
    }

    @Override
    public Single<List<Recipe>> getAllRecipes() {
        return service.getAll();
    }
}
