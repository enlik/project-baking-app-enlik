package enliktjioe.id.bakingapp.main.recipe.step;

import enliktjioe.id.bakingapp.data.model.StepItem;


public interface RecipeStepContract {
    interface View {
        void pausePlayer();

        void requestCurrentVideoPosition();

        void enablePreviousButton();

        void disablePreviousButton();

        void enableNextButton();

        void disableNextButton();

        void hideExoPlayer();

        void showThumbnailImage(String thumbnailURL);

        void showShortDescription(String shortDescription);

        void clearShortDescription(String s);

        void clearDescription(String description);

        void showDescription(String description);

        void playVideo(String videoURL, long currentVideoPosition);
    }

    interface Presenter {
        void bindView(View view, StepItem stepItem, boolean enablePreviousButton, boolean enableNextButton);

        void viewCreated();

        void onPause();

        void onResume();

        void setCurrentVideoPosition(long currentVideoPosition);

        void onNextClick();

        void onPrevClick();

        void bindView(View view);
    }
}
