package enliktjioe.id.bakingapp.main.recipes.items;

import enliktjioe.id.bakingapp.main.recipes.OnRecipeClick;
import enliktjioe.id.bakingapp.main.view.ViewHolder;

public class RecipeItemViewHolder extends ViewHolder<RecipeItemView> {

    public final OnRecipeClick onRecipeClick;

    public RecipeItemViewHolder(RecipeItemView itemView, OnRecipeClick onRecipeClick) {
        super(itemView);

        this.onRecipeClick = onRecipeClick;
    }
}
