package enliktjioe.id.bakingapp.main.recipe.summary.items;

import enliktjioe.id.bakingapp.data.model.StepItem;



public interface OnStepClickListener {

    void onStepClick(StepItem stepItem);
}
