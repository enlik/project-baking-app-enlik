package enliktjioe.id.bakingapp.main.recipe.summary.items;

import enliktjioe.id.bakingapp.data.model.StepItem;

public class OnStepClickEvent {

    public StepItem stepItem;

    public OnStepClickEvent(StepItem stepItem) {
        this.stepItem = stepItem;
    }
}
