package enliktjioe.id.bakingapp.main.recipes;


import java.util.ArrayList;
import java.util.List;

import icepick.State;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import enliktjioe.id.bakingapp.data.model.Recipe;
import enliktjioe.id.bakingapp.utilities.usecase.GetRecipesUseCase;

class RecipesPresenter implements RecipesContract.Presenter {

    private final GetRecipesUseCase getRecipesUseCase;
    private RecipesContract.View view;

    @State
    ArrayList<Recipe> recipes;

    public RecipesPresenter(GetRecipesUseCase getRecipesUseCase) {
        this.getRecipesUseCase = getRecipesUseCase;
    }

    @Override
    public void bindView(final RecipesContract.View view) {
        this.view = view;
    }

    @Override
    public void viewCreated() {
        if (recipes == null || recipes.size() == 0) {
            loadData();
        } else {
            view.hideLoading();
            view.showRecipes(recipes);
            view.hideError();
        }
    }

    @Override
    public void paused() {
        getRecipesUseCase.unsubscribe();
    }

    @Override
    public void onRecipeClick(Recipe recipe) {
        view.showRecipe(recipe);
    }

    @Override
    public void retryButtonClicked() {
        loadData();
    }

    private void loadData() {
        view.hideRecipes();
        view.hideError();
        view.showLoading();

        getRecipesUseCase.execute(new Consumer<List<Recipe>>() {
            @Override
            public void accept(@NonNull List<Recipe> recipes) throws Exception {
                RecipesPresenter.this.recipes = new ArrayList<>(recipes);

                view.hideLoading();
                view.showRecipes(recipes);
                view.hideError();
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(@NonNull Throwable throwable) throws Exception {
                view.hideLoading();
                view.hideRecipes();
                view.showError();
            }
        });
    }
}
