package enliktjioe.id.bakingapp.main.recipe;

import enliktjioe.id.bakingapp.data.model.Recipe;
import enliktjioe.id.bakingapp.data.model.StepItem;


public interface RecipeContract {
    interface View {

        void showRecipeSummaryFragment(Recipe recipe);

        void showRecipeStepFragment(StepItem stepItem, boolean enablePreviousButton, boolean enableNextButton, boolean addToBackstack);

        void setTitle(String name);
    }

    interface Presenter {

        void viewResumed();

        void viewPaused();

        void recipeSummaryFragmentCreated();

        void recipeStepFragmentCreated();

        void viewCreated();
    }
}
