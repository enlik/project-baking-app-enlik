package enliktjioe.id.bakingapp;

import android.app.Application;

import timber.log.Timber;


public class BaseActivity extends Application {

    private static BaseComponent GRAPH;
    private static BaseActivity instance;

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }

    public static BaseComponent getGraph() {
        if (GRAPH == null) {
            GRAPH = BaseComponent.Initializer.init(instance);
        }

        return GRAPH;
    }

    public void setGraph(BaseComponent graph) {
        GRAPH = graph;
    }
}
