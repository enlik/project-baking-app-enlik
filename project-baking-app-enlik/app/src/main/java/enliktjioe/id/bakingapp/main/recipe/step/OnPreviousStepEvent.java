package enliktjioe.id.bakingapp.main.recipe.step;

import enliktjioe.id.bakingapp.data.model.StepItem;

public class OnPreviousStepEvent {
    public StepItem stepItem;

    public OnPreviousStepEvent(StepItem stepItem) {

        this.stepItem = stepItem;
    }
}
