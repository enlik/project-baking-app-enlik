package enliktjioe.id.bakingapp.data.repo.recipes;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import enliktjioe.id.bakingapp.data.model.Recipe;
import enliktjioe.id.bakingapp.data.RetrofitRecipesRepository;

import static enliktjioe.id.bakingapp.BaseActivity.getGraph;


public class RecipesRepositoryImpl implements RecipesRepository {

    @Inject RetrofitRecipesRepository retrofitRecipesRepository;

    public RecipesRepositoryImpl() {
        getGraph().inject(this);
    }

    @Override
    public Single<List<Recipe>> getAllRecipes() {
        return retrofitRecipesRepository.getAllRecipes();
    }

}
