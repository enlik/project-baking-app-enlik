package enliktjioe.id.bakingapp.main.recipe.summary;

import android.os.Parcelable;

import com.hwangjr.rxbus.Bus;

import icepick.State;
import enliktjioe.id.bakingapp.data.model.Recipe;
import enliktjioe.id.bakingapp.data.model.StepItem;
import enliktjioe.id.bakingapp.main.recipe.summary.items.OnStepClickEvent;


public class RecipeSummaryPresenter implements RecipeSummaryContract.Presenter {

    private final RecipeSummaryContract.View view;
    private final Bus bus;

    @State Recipe recipe;
    @State Parcelable recyclerState;
    @State Integer selectedPosition;

    public RecipeSummaryPresenter(RecipeSummaryContract.View view, int selectedPosition, Bus bus) {
        this.selectedPosition = selectedPosition;
        this.view = view;
        this.bus = bus;
    }

    @Override
    public void setRecyclerState(Parcelable recyclerState) {
        this.recyclerState = recyclerState;
    }

    @Override
    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }

    @Override
    public void viewStarted() {
        if (recipe == null) {
            return;
        }

        view.showRecipes(recipe, selectedPosition);

        if (recyclerState != null) {
            view.updateRecyclerViewState(recyclerState);
        }
    }

    @Override
    public void onStepClick(StepItem stepItem) {
        selectedPosition = recipe.steps().indexOf(stepItem) + 1;
        bus.post(new OnStepClickEvent(stepItem));
    }
}
