package enliktjioe.id.bakingapp.main.recipe;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.f2prateek.dart.Dart;
import com.f2prateek.dart.InjectExtra;
import com.hwangjr.rxbus.Bus;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import enliktjioe.id.bakingapp.R;
import enliktjioe.id.bakingapp.data.model.Recipe;
import enliktjioe.id.bakingapp.data.model.StepItem;
import enliktjioe.id.bakingapp.main.recipe.step.RecipeStepFragment;
import enliktjioe.id.bakingapp.main.recipe.summary.RecipeSummaryFragment;

import static enliktjioe.id.bakingapp.BaseActivity.getGraph;

public class RecipeActivity extends AppCompatActivity implements RecipeContract.View {

    public static final String EXTRA_RECIPE = "EXTRA_RECIPE";

    @InjectExtra(EXTRA_RECIPE)
    Recipe recipe;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Nullable
    @BindView(R.id.recipe_layout)
    FrameLayout firstFrameLayout;

    @Inject Bus bus;

    RecipeContract.Presenter presenter;

    @Override
    protected void onResume() {
        super.onResume();

        presenter.viewResumed();
    }

    @Override
    protected void onPause() {
        presenter.viewPaused();

        super.onPause();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getGraph().inject(this);

        setContentView(R.layout.activity_recipe);
        ButterKnife.bind(this);
        Dart.inject(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        if (presenter == null) {
            presenter = new RecipePresenter(this, recipe, bus);
        }

        RecipeSummaryFragment recipeSummaryFragment = (RecipeSummaryFragment) getSupportFragmentManager().findFragmentById(R.id.activity_recipe_recipe_summary_recipe_summary_fragment);
        if (savedInstanceState == null) {
            if (recipeSummaryFragment == null) {
                recipeSummaryFragment = new RecipeSummaryFragment();
                getSupportFragmentManager().beginTransaction()
                        .add(firstFrameLayout.getId(), recipeSummaryFragment)
                        .commit();
                getSupportFragmentManager().executePendingTransactions();
            }
            presenter.recipeSummaryFragmentCreated();

            RecipeStepFragment recipeStepFragment = (RecipeStepFragment) getSupportFragmentManager().findFragmentById(R.id.activity_recipe_recipe_summary_recipe_step_fragment);
            if (recipeStepFragment != null) {
                presenter.recipeStepFragmentCreated();
            }
        }
        presenter.viewCreated();
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().beginTransaction()
                    .remove(getSupportFragmentManager().findFragmentById(firstFrameLayout.getId()))
                    .commit();
            getSupportFragmentManager().popBackStack();
            return;
        }

        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                    getSupportFragmentManager().beginTransaction()
                            .remove(getSupportFragmentManager().findFragmentById(firstFrameLayout.getId()))
                            .commit();
                    getSupportFragmentManager().popBackStack();
                    return true;
                }

                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showRecipeSummaryFragment(Recipe recipe) {
        RecipeSummaryFragment recipeSummaryFragment = (RecipeSummaryFragment) getSupportFragmentManager().findFragmentById(R.id.activity_recipe_recipe_summary_recipe_summary_fragment);
        if (recipeSummaryFragment == null) {
            recipeSummaryFragment = new RecipeSummaryFragment();

            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(firstFrameLayout.getId(), recipeSummaryFragment);
            ft.commit();
            getSupportFragmentManager().executePendingTransactions();
        }

        recipeSummaryFragment.bindView(recipe);
    }

    @Override
    public void showRecipeStepFragment(StepItem stepItem, boolean enablePreviousButton, boolean enableNextButton, boolean addToBackstack) {
        RecipeStepFragment recipeStepFragment = (RecipeStepFragment) getSupportFragmentManager().findFragmentById(R.id.activity_recipe_recipe_summary_recipe_step_fragment);
        if (recipeStepFragment == null) {
            recipeStepFragment = new RecipeStepFragment();

            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(firstFrameLayout.getId(), recipeStepFragment);
            if (addToBackstack) {
                ft.addToBackStack("recipeSummaryFragment");
            }
            ft.commit();
            getSupportFragmentManager().executePendingTransactions();
        }

        recipeStepFragment.bindView(stepItem, enablePreviousButton, enableNextButton);
    }

    @Override
    public void setTitle(String name) {
        if (recipe != null && recipe.name() != null) {
            super.setTitle(recipe.name());
        }
    }
}
