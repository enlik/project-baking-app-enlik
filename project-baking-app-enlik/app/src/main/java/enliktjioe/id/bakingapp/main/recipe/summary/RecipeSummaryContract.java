package enliktjioe.id.bakingapp.main.recipe.summary;

import android.os.Parcelable;

import enliktjioe.id.bakingapp.data.model.Recipe;
import enliktjioe.id.bakingapp.data.model.StepItem;


public interface RecipeSummaryContract {
    interface View {

        void updateRecyclerViewState(Parcelable recyclerState);

        void showRecipes(Recipe recipe, int selectedPosition);
    }

    interface Presenter {

        void setRecyclerState(Parcelable recyclerState);

        void setRecipe(Recipe recipe);

        void viewStarted();

        void onStepClick(StepItem stepItem);
    }
}
