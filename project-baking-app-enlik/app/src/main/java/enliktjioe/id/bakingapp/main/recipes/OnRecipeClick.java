package enliktjioe.id.bakingapp.main.recipes;


import enliktjioe.id.bakingapp.data.model.Recipe;

public interface OnRecipeClick {
    void onRecipeClick(Recipe recipe);
}
