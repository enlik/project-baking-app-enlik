package enliktjioe.id.bakingapp.utilities.threading;


import io.reactivex.Scheduler;

public interface UseCaseExecutor {
    Scheduler getScheduler();
}