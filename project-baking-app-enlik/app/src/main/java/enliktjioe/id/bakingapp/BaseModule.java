package enliktjioe.id.bakingapp;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hwangjr.rxbus.Bus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import enliktjioe.id.bakingapp.data.RecipesCache;
import enliktjioe.id.bakingapp.data.repo.recipes.RecipesRepository;
import enliktjioe.id.bakingapp.data.repo.recipes.RecipesRepositoryImpl;
import enliktjioe.id.bakingapp.data.AutoValueGsonTypeAdapterFactory;
import enliktjioe.id.bakingapp.utilities.threading.PostExecutionThread;
import enliktjioe.id.bakingapp.utilities.threading.UseCaseExecutor;
import enliktjioe.id.bakingapp.utilities.usecase.GetRecipesUseCase;

@Module
public class BaseModule {
    private final Context context;

    public BaseModule(Context context) {
        this.context = context;
    }

    @Provides
    public PostExecutionThread providePostExecutionThread() {
        return new PostExecutionThread() {
            @Override
            public Scheduler getScheduler() {
                return AndroidSchedulers.mainThread();
            }
        };
    }

    @Provides
    public UseCaseExecutor provideUseCaseExecutor() {
        return new UseCaseExecutor() {
            @Override
            public Scheduler getScheduler() {
                return Schedulers.io();
            }
        };
    }


    @Provides
    public Context provideContext() {
        return context;
    }

    @Singleton
    @Provides
    public Gson provideGson() {
        return new GsonBuilder()
                .registerTypeAdapterFactory(AutoValueGsonTypeAdapterFactory.create())
                .create();
    }

    @Singleton
    @Provides
    public RecipesRepository provideRecipesRepository() {
        return new RecipesRepositoryImpl();
    }

    @Singleton
    @Provides
    public Bus providesBus() {
        return new Bus();
    }

    @Singleton
    @Provides
    public RecipesCache providesRecipesCache() {
        return new RecipesCache();
    }

    @Provides
    public GetRecipesUseCase provideRecipesUseCase(UseCaseExecutor useCaseExecutor, PostExecutionThread postExecutionThread, RecipesRepository recipesRepository, RecipesCache recipesCache) {
        return new GetRecipesUseCase(useCaseExecutor, postExecutionThread, recipesRepository, recipesCache);
    }

}
