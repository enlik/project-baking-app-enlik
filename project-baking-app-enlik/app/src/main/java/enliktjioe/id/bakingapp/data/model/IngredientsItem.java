package enliktjioe.id.bakingapp.data.model;

import android.os.Parcelable;
import android.support.annotation.Nullable;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;

@AutoValue
public abstract class IngredientsItem implements Parcelable {

    @Nullable
    @SerializedName("quantity")
    public abstract Float quantity();

    @Nullable
    @SerializedName("measure")
    public abstract String measure();

    @Nullable
    @SerializedName("ingredient")
    public abstract String ingredient();

    public static TypeAdapter<IngredientsItem> typeAdapter(Gson gson) {
        return new AutoValue_IngredientsItem.GsonTypeAdapter(gson);
    }

    public static IngredientsItem create() {
        return new AutoValue_IngredientsItem(null, null, null);
    }

    public static IngredientsItem create(Float quantity, String measure, String ingredient) {
        return new AutoValue_IngredientsItem(quantity, measure, ingredient);
    }
}