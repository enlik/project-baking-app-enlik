package enliktjioe.id.bakingapp.main.recipe;

import com.hwangjr.rxbus.Bus;
import com.hwangjr.rxbus.annotation.Subscribe;

import enliktjioe.id.bakingapp.data.model.Recipe;
import enliktjioe.id.bakingapp.data.model.StepItem;
import enliktjioe.id.bakingapp.main.recipe.step.OnNextStepEvent;
import enliktjioe.id.bakingapp.main.recipe.step.OnPreviousStepEvent;
import enliktjioe.id.bakingapp.main.recipe.summary.items.OnStepClickEvent;


class RecipePresenter implements RecipeContract.Presenter {
    private RecipeContract.View view;
    private Bus bus;

    Recipe recipe;

    public RecipePresenter(RecipeContract.View view, Recipe recipe, Bus bus) {
        this.view = view;
        this.recipe = recipe;
        this.bus = bus;
    }

    @Override
    public void viewResumed() {
        bus.register(this);
    }

    @Override
    public void viewPaused() {
        bus.unregister(this);
    }

    @Override
    public void recipeSummaryFragmentCreated() {
        view.showRecipeSummaryFragment(recipe);
    }

    @Override
    public void recipeStepFragmentCreated() {
        view.showRecipeStepFragment(recipe.steps().get(0), false, true, false);
    }

    @Override
    public void viewCreated() {
        view.setTitle(recipe.name());
    }

    private boolean allowPreviousButton(Recipe recipe, StepItem stepItem) {
        return recipe.steps().indexOf(stepItem) >= 1;
    }

    private boolean allowNextButton(Recipe recipe, StepItem stepItem) {
        return recipe.steps().indexOf(stepItem) < recipe.steps().size() - 1;
    }

    @Subscribe
    public void onNextStepClicked(final OnNextStepEvent onNextStepEvent) {
        StepItem item = recipe.steps().get(recipe.steps().indexOf(onNextStepEvent.stepItem) + 1);

        boolean allowPrevious = allowPreviousButton(recipe, item);
        boolean allowNext = allowNextButton(recipe, item);

        view.showRecipeStepFragment(item, allowPrevious, allowNext, false);
    }

    @Subscribe
    public void onPreviousStepClickListener(final OnPreviousStepEvent onPreviousStepEvent) {
        StepItem item = recipe.steps().get(recipe.steps().indexOf(onPreviousStepEvent.stepItem) - 1);

        boolean allowPrevious = allowPreviousButton(recipe, item);
        boolean allowNext = allowNextButton(recipe, item);

        view.showRecipeStepFragment(item, allowPrevious, allowNext, false);
    }

    @Subscribe
    public void onStepClick(final OnStepClickEvent onStepClick) {
        boolean allowPrevious = allowPreviousButton(recipe, onStepClick.stepItem);
        boolean allowNext = allowNextButton(recipe, onStepClick.stepItem);

        view.showRecipeStepFragment(onStepClick.stepItem, allowPrevious, allowNext, true);
    }
}
