package enliktjioe.id.bakingapp.data.model;

import android.os.Parcelable;
import android.support.annotation.Nullable;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;

@AutoValue
public abstract class StepItem implements Parcelable {

    @Nullable
    @SerializedName("videoURL")
    public abstract String videoURL();

    @Nullable
    @SerializedName("description")
    public abstract String description();

    @Nullable
    @SerializedName("id")
    public abstract Integer id();

    @Nullable
    @SerializedName("shortDescription")
    public abstract String shortDescription();

    @Nullable
    @SerializedName("thumbnailURL")
    public abstract String thumbnailURL();

    public static TypeAdapter<StepItem> typeAdapter(Gson gson) {
        return new AutoValue_StepItem.GsonTypeAdapter(gson);
    }

    public static StepItem create() {
        return new AutoValue_StepItem(null, null, null, null, null);
    }

    public static StepItem create(String videoUrl, String description, Integer id, String shortDescription, String thumbnailURL) {
        return new AutoValue_StepItem(videoUrl, description, id, shortDescription, thumbnailURL);
    }
}