package enliktjioe.id.bakingapp.main.recipe.summary;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import enliktjioe.id.bakingapp.R;
import enliktjioe.id.bakingapp.data.model.Recipe;
import enliktjioe.id.bakingapp.data.model.StepItem;
import enliktjioe.id.bakingapp.main.recipe.summary.items.OnStepClickListener;
import enliktjioe.id.bakingapp.main.recipe.summary.items.RecipeSummaryIngredientsView;
import enliktjioe.id.bakingapp.main.recipe.summary.items.RecipeSummaryIngredientsViewHolder;
import enliktjioe.id.bakingapp.main.recipe.summary.items.RecipeSummaryItemViewHolder;
import enliktjioe.id.bakingapp.main.recipe.summary.items.RecipeSummaryStepsView;
import enliktjioe.id.bakingapp.main.recipe.summary.items.RecipeSummaryStepsViewHolder;

class RecipeSummaryAdapter extends RecyclerView.Adapter<RecipeSummaryItemViewHolder> {

    private Recipe recipe;
    private OnStepClickListener onStepClickListener;

    private static final int VIEW_TYPE_INGREDIENTS = 0;
    private static final int VIEW_TYPE_STEPS = 1;
    private int selectedPosition = -1;
    private boolean handleSelectedPosition;

    public RecipeSummaryAdapter(final Recipe recipe, final OnStepClickListener onStepClickListener, int selectedPosition, final boolean handleSelectedPosition) {
        this.recipe = recipe;
        this.selectedPosition = selectedPosition;
        this.handleSelectedPosition = handleSelectedPosition;

        this.onStepClickListener = new OnStepClickListener() {
            @Override
            public void onStepClick(StepItem stepItem) {
                if (handleSelectedPosition) {
                    int oldSelectedPosition = RecipeSummaryAdapter.this.selectedPosition;
                    RecipeSummaryAdapter.this.selectedPosition = recipe.steps().indexOf(stepItem) + 1;
                    onStepClickListener.onStepClick(stepItem);
                    notifyItemChanged(RecipeSummaryAdapter.this.selectedPosition);
                    notifyItemChanged(oldSelectedPosition);
                } else {
                    onStepClickListener.onStepClick(stepItem);
                }
            }
        };
    }

    @Override
    public RecipeSummaryItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType) {
            case VIEW_TYPE_INGREDIENTS:
                RecipeSummaryIngredientsView recipeSummaryIngredientsView = (RecipeSummaryIngredientsView) LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.recipe_recipe_summary_ingredients_item, parent, false);

                return new RecipeSummaryIngredientsViewHolder(recipeSummaryIngredientsView);
            case VIEW_TYPE_STEPS:
                RecipeSummaryStepsView recipeSummaryStepsView = (RecipeSummaryStepsView) LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.recipe_recipe_summary_step_item, parent, false);

                return new RecipeSummaryStepsViewHolder(recipeSummaryStepsView);
        }
        return null;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return VIEW_TYPE_INGREDIENTS;
        } else {
            return VIEW_TYPE_STEPS;
        }
    }


    @Override
    public void onBindViewHolder(RecipeSummaryItemViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case VIEW_TYPE_INGREDIENTS:
                ((RecipeSummaryIngredientsViewHolder) holder).getView().bind(recipe.ingredients());
                break;
            case VIEW_TYPE_STEPS:

                boolean isSelected = false;
                if (handleSelectedPosition) {
                    if (selectedPosition == position) {
                        isSelected = true;
                    }
                }

                ((RecipeSummaryStepsViewHolder) holder).getView().bind(recipe.steps().get(position - 1), onStepClickListener, isSelected);



                break;
        }
    }

    @Override
    public int getItemCount() {
        return 1 + (recipe.steps() != null ? recipe.steps().size() : 0);
    }
}
