package enliktjioe.id.bakingapp.main.view;

import android.support.v7.widget.RecyclerView;
import android.view.View;


public class ViewHolder<T extends View> extends RecyclerView.ViewHolder {

    public ViewHolder(T itemView) {
        super(itemView);
    }

    @SuppressWarnings("unchecked")
    public T getView() {
        return (T) itemView;
    }
}
