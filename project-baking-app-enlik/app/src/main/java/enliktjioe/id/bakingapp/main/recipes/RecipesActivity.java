package enliktjioe.id.bakingapp.main.recipes;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import icepick.Icepick;
import icepick.State;
import enliktjioe.id.bakingapp.R;
import enliktjioe.id.bakingapp.data.model.Recipe;
import enliktjioe.id.bakingapp.utilities.usecase.GetRecipesUseCase;
import enliktjioe.id.bakingapp.main.recipe.RecipeActivity;

import static enliktjioe.id.bakingapp.BaseActivity.getGraph;

public class RecipesActivity extends AppCompatActivity implements RecipesContract.View, OnRecipeClick {

    @BindView(R.id.activity_recipes_error_container)
    ViewGroup errorContainer;

    @BindView(R.id.loading)
    ProgressBar loadingProgressBar;

    @BindView(R.id.recycler_view)
    RecyclerView recipesRecyclerView;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Inject
    GetRecipesUseCase getRecipesUseCase;

    @State
    Parcelable recyclerState;

    RecipesContract.Presenter recipesPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getGraph().inject(this);

        setTitle(getString(R.string.title_text));

        setContentView(R.layout.activity_recipes);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        if (recipesPresenter == null) {
            recipesPresenter = new RecipesPresenter(getRecipesUseCase);
        }
        Icepick.restoreInstanceState(recipesPresenter, savedInstanceState);
        Icepick.restoreInstanceState(this, savedInstanceState);

        recipesPresenter.bindView(this);
        recipesPresenter.viewCreated();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (recipesRecyclerView != null && recipesRecyclerView.getLayoutManager() != null) {
            recyclerState = recipesRecyclerView.getLayoutManager().onSaveInstanceState();
        }
        super.onSaveInstanceState(outState);
        Icepick.saveInstanceState(recipesPresenter, outState);
        Icepick.saveInstanceState(this, outState);
    }

    @Override
    protected void onPause() {
        recipesPresenter.paused();
        super.onPause();
    }

    @Override
    public void hideRecipes() {
        recipesRecyclerView.setVisibility(View.GONE);
    }

    @Override
    public void hideError() {
        errorContainer.setVisibility(View.GONE);
    }

    @Override
    public void hideLoading() {
        loadingProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void showLoading() {
        loadingProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void showRecipes(List<Recipe> recipes) {
        recipesRecyclerView.setVisibility(View.VISIBLE);
        RecipesAdapter recipesAdapter = new RecipesAdapter(recipes, this);
        recipesRecyclerView.setLayoutManager(new GridLayoutManager(this, getResources().getInteger(R.integer.recipes_span_count)));
        recipesRecyclerView.setAdapter(recipesAdapter);
        recipesAdapter.notifyDataSetChanged();
        if (recyclerState != null) {
            recipesRecyclerView.getLayoutManager().onRestoreInstanceState(recyclerState);
        }
    }

    @Override
    public void showError() {
        errorContainer.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.error_container)
    public void onRetryButtonClick() {
        recipesPresenter.retryButtonClicked();
    }

    @Override
    public void onRecipeClick(Recipe recipe) {
        recipesPresenter.onRecipeClick(recipe);
    }

    @Override
    public void showRecipe(Recipe recipe) {
        Intent intent = new Intent(this, RecipeActivity.class);
        intent.putExtra(RecipeActivity.EXTRA_RECIPE, recipe);
        startActivity(intent);
    }
}
