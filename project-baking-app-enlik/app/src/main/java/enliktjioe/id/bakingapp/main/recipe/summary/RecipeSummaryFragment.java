package enliktjioe.id.bakingapp.main.recipe.summary;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hwangjr.rxbus.Bus;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import icepick.Icepick;
import enliktjioe.id.bakingapp.R;
import enliktjioe.id.bakingapp.data.model.Recipe;
import enliktjioe.id.bakingapp.data.model.StepItem;
import enliktjioe.id.bakingapp.main.recipe.summary.items.OnStepClickListener;

import static enliktjioe.id.bakingapp.BaseActivity.getGraph;

public class RecipeSummaryFragment extends Fragment implements RecipeSummaryContract.View {

    @BindView(R.id.fragment_summary_view)
    RecyclerView recyclerView;

    @Inject Bus bus;


    RecipeSummaryContract.Presenter presenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recipe_summary, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getGraph().inject(this);

        if (presenter == null) {
            presenter = new RecipeSummaryPresenter(this, getResources().getBoolean(R.bool.isTablet) ? 1 : -1, bus);
        }
    }

    @Override
    public void onPause() {
        if (recyclerView != null && recyclerView.getLayoutManager() != null) {
            presenter.setRecyclerState(recyclerView.getLayoutManager().onSaveInstanceState());
        }

        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Icepick.saveInstanceState(this, outState);
        Icepick.saveInstanceState(presenter, outState);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        Icepick.restoreInstanceState(this, savedInstanceState);
        Icepick.restoreInstanceState(presenter, savedInstanceState);
    }

    public void bindView(Recipe recipe) {
        presenter.setRecipe(recipe);
    }

    @Override
    public void onStart() {
        super.onStart();

        presenter.viewStarted();
    }

    @Override
    public void updateRecyclerViewState(Parcelable recyclerState) {
        if (recyclerView != null && recyclerView.getLayoutManager() != null) {
            recyclerView.getLayoutManager().onRestoreInstanceState(recyclerState);
        }
    }

    @Override
    public void showRecipes(final Recipe recipe, int selectedPosition) {
        if (recyclerView.getLayoutManager() == null) {
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        }


        if (recyclerView.getAdapter() == null) {
            RecipeSummaryAdapter recipeSummaryAdapter = new RecipeSummaryAdapter(recipe, new OnStepClickListener() {
                @Override
                public void onStepClick(StepItem stepItem) {
                    presenter.onStepClick(stepItem);

                }
            }, selectedPosition, getResources().getBoolean(R.bool.isTablet));
            recyclerView.setAdapter(recipeSummaryAdapter);
        }
    }
}
