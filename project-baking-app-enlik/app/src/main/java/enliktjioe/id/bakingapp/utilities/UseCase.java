package enliktjioe.id.bakingapp.utilities;


import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import enliktjioe.id.bakingapp.utilities.threading.PostExecutionThread;
import enliktjioe.id.bakingapp.utilities.threading.UseCaseExecutor;

public abstract class UseCase<T> implements Interactor<T> {

    private final PostExecutionThread postExecutionThread;
    private final UseCaseExecutor useCaseExecutor;
    private Disposable disposable;

    public UseCase(UseCaseExecutor useCaseExecutor, PostExecutionThread postExecutionThread) {
        this.useCaseExecutor = useCaseExecutor;
        this.postExecutionThread = postExecutionThread;
    }

    @Override
    public void execute(Consumer<T> consumer, Consumer<Throwable> errorConsumer) {
        this.disposable = createObservable()
                .subscribeOn(useCaseExecutor.getScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(consumer, errorConsumer);
    }

    @Override
    public Observable<T> execute() {
        return createObservable();
    }

    protected abstract Observable<T> createObservable();

    public void unsubscribe() {
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }
    }
}
