package enliktjioe.id.bakingapp.main.recipes.items;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import enliktjioe.id.bakingapp.R;
import enliktjioe.id.bakingapp.data.model.Recipe;

public class RecipeItemView extends RelativeLayout {

    @BindView(R.id.recipe_imageview)
    public AppCompatImageView recipeImageView;

    @BindView(R.id.recipe_item_textview)
    public AppCompatTextView textTextView;

    public RecipeItemView(Context context) {
        this(context, null);
    }

    public RecipeItemView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RecipeItemView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        LayoutInflater.from(context).inflate(R.layout.recipes_recipe_item, this);
        ButterKnife.bind(this);
    }

    public void bind(Recipe recipe) {
        Glide.with(getContext())
                .load(recipe.image())
                .fitCenter()
                .into(recipeImageView);
        textTextView.setText(recipe.name());
    }

}
