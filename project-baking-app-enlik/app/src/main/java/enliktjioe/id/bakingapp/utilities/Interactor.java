package enliktjioe.id.bakingapp.utilities;

import io.reactivex.Observable;
import io.reactivex.functions.Consumer;


interface Interactor<T> {
    void execute(Consumer<T> consumer, Consumer<Throwable> errorConsumer);

    Observable<T> execute();
}
