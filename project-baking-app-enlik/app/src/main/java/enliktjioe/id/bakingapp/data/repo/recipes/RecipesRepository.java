package enliktjioe.id.bakingapp.data.repo.recipes;


import java.util.List;

import io.reactivex.Single;
import enliktjioe.id.bakingapp.data.model.Recipe;

public interface RecipesRepository {
    Single<List<Recipe>> getAllRecipes();
}
