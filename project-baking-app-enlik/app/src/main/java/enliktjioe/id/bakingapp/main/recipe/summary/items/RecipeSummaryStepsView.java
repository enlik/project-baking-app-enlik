package enliktjioe.id.bakingapp.main.recipe.summary.items;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import enliktjioe.id.bakingapp.R;
import enliktjioe.id.bakingapp.data.model.StepItem;


public class RecipeSummaryStepsView extends RelativeLayout {

    @BindView(R.id.summary_step_item_tv)
    AppCompatTextView tv;

    @BindView(R.id.summary_step_cardview)
    CardView cardView;

    public RecipeSummaryStepsView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RecipeSummaryStepsView(Context context) {
        this(context, null, 0);
    }

    public RecipeSummaryStepsView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        ButterKnife.bind(this);
    }

    public void bind(final StepItem stepItem, final OnStepClickListener onStepClickListener, boolean isSelected) {
        tv.setText(stepItem.shortDescription());
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                onStepClickListener.onStepClick(stepItem);
            }
        });

        if (isSelected) {
            cardView.setBackgroundResource(R.drawable.selected_card_background);
        } else {
            cardView.setBackgroundResource(R.drawable.empty_card_background);
        }
    }
}
