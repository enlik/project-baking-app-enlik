package enliktjioe.id.bakingapp.main.recipe.step;


import com.hwangjr.rxbus.Bus;

import icepick.State;
import enliktjioe.id.bakingapp.data.model.StepItem;

class RecipeStepPresenter implements RecipeStepContract.Presenter {

    private final Bus bus;

    private RecipeStepContract.View view;

    @State StepItem stepItem;
    @State long currentVideoPosition;
    @State boolean enablePreviousButton;
    @State boolean enableNextButton;

    public RecipeStepPresenter(Bus bus) {
        this.bus = bus;
    }


    private void displayInformations() {


        if (enablePreviousButton) {
            view.enablePreviousButton();
        } else {
            view.disablePreviousButton();
        }

        if (enableNextButton) {
            view.enableNextButton();
        } else {
            view.disableNextButton();
        }
        view.hideExoPlayer();

        view.clearShortDescription(stepItem.shortDescription());
        view.clearDescription(stepItem.description());

        if (stepItem.thumbnailURL() != null){
            view.showThumbnailImage(stepItem.thumbnailURL());
        }

        if (stepItem.shortDescription() != null) {
            view.showShortDescription(stepItem.shortDescription());
        }

        if (stepItem.description() != null) {
            view.showDescription(stepItem.description());
        }

        if (stepItem.videoURL() != null && !"".equals(stepItem.videoURL())) {
            view.playVideo(stepItem.videoURL(), currentVideoPosition);
        }

    }

    @Override
    public void bindView(RecipeStepContract.View view, StepItem stepItem, boolean enablePreviousButton, boolean enableNextButton) {
        this.view = view;
        this.stepItem = stepItem;
        this.enablePreviousButton = enablePreviousButton;
        this.enableNextButton = enableNextButton;

        view.pausePlayer();
        currentVideoPosition = 0;

        displayInformations();
    }

    @Override
    public void viewCreated() {

    }


    @Override
    public void onPause() {
        if (view != null) {
            view.pausePlayer();
            view.requestCurrentVideoPosition();
        }
    }

    @Override
    public void onResume() {

        if (view != null && stepItem != null) {
            displayInformations();
        }
    }

    @Override
    public void setCurrentVideoPosition(long currentVideoPosition) {
        this.currentVideoPosition = currentVideoPosition;
    }

    @Override
    public void onNextClick() {
        bus.post(new OnNextStepEvent(stepItem));
    }

    @Override
    public void onPrevClick() {
        bus.post(new OnPreviousStepEvent(stepItem));
    }

    @Override
    public void bindView(RecipeStepContract.View view) {
        this.view = view;
    }
}
