package enliktjioe.id.bakingapp.main.recipe.step;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.TransferListener;
import com.google.android.exoplayer2.util.Util;
import com.hwangjr.rxbus.Bus;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import icepick.Icepick;
import enliktjioe.id.bakingapp.R;
import enliktjioe.id.bakingapp.data.model.StepItem;

import static com.google.android.exoplayer2.ExoPlayer.STATE_READY;
import static enliktjioe.id.bakingapp.BaseActivity.getGraph;

public class RecipeStepFragment extends Fragment implements RecipeStepContract.View {

    @Nullable
    @BindView(R.id.step_thumbnail_image)
    ImageView stepThumbnailImage;

    @Nullable
    @BindView(R.id.step_tv)
    TextView descriptionTextView;

    @Nullable
    @BindView(R.id.step_short_tv)
    TextView shortDescriptionTextView;

    @BindView(R.id.step_video)
    SimpleExoPlayerView simpleExoPlayerView;

    @Nullable
    @BindView(R.id.previous_button)
    Button prevButton;

    @Nullable
    @BindView(R.id.next_button)
    Button nextButton;

    private SimpleExoPlayer player;

    @Inject Bus bus;

    private RecipeStepContract.Presenter recipeStepPresenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recipe_step, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getGraph().inject(this);

        if (recipeStepPresenter == null) {
            recipeStepPresenter = new RecipeStepPresenter(bus);
            recipeStepPresenter.bindView(this);
        }
        Icepick.restoreInstanceState(recipeStepPresenter, savedInstanceState);
        Icepick.restoreInstanceState(this, savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Icepick.saveInstanceState(this, outState);
        Icepick.saveInstanceState(recipeStepPresenter, outState);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        Icepick.restoreInstanceState(this, savedInstanceState);
        Icepick.restoreInstanceState(recipeStepPresenter, savedInstanceState);
    }

    @Override
    public void onPause() {
        recipeStepPresenter.onPause();
        super.onPause();
        releasePlayer();
    }

    @Override
    public void onStop() {
        super.onStop();
        releasePlayer();
    }


    @Override
    public void onResume() {
        super.onResume();

        recipeStepPresenter.bindView(this);
        recipeStepPresenter.onResume();
    }

    public void bindView(StepItem stepItem, boolean enablePreviousButton, boolean enableNextButton) {
        recipeStepPresenter.bindView(this, stepItem, enablePreviousButton, enableNextButton);
        recipeStepPresenter.viewCreated();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (player != null) {
            player.stop();
        }
        releasePlayer();
    }

    public void releasePlayer () {
        if (player != null) {
            player.release();
            player = null;
        }
    }

    @Optional
    @OnClick(R.id.next_button)
    public void onNextClick() {
        recipeStepPresenter.onNextClick();
    }


    @Optional
    @OnClick(R.id.previous_button)
    public void onPrevClick() {
        recipeStepPresenter.onPrevClick();
    }

    @Override
    public void pausePlayer() {
        if (player != null) {
            player.stop();
        }
    }

    @Override
    public void requestCurrentVideoPosition() {
        if (player != null) {
            recipeStepPresenter.setCurrentVideoPosition(player.getCurrentPosition());
        }
    }

    @Override
    public void enablePreviousButton() {
        if (prevButton != null) {
            prevButton.setEnabled(true);
            prevButton.setAlpha(1);
        }
    }

    @Override
    public void disablePreviousButton() {
        if (prevButton != null) {
            prevButton.setEnabled(false);
            prevButton.setAlpha(0.7f);
        }
    }

    @Override
    public void enableNextButton() {
        if (nextButton != null) {
            nextButton.setEnabled(true);
            nextButton.setAlpha(1);
        }
    }

    @Override
    public void disableNextButton() {
        if (nextButton != null) {
            nextButton.setEnabled(false);
            nextButton.setAlpha(0.7f);
        }
    }

    @Override
    public void hideExoPlayer() {
        if (simpleExoPlayerView != null) {
            simpleExoPlayerView.setVisibility(View.GONE);
        }
    }

    public void showThumbnailImage(String thumbnailURL) {
        if (thumbnailURL != null){
            Glide.with(getContext())
                    .load(thumbnailURL)
                    .fitCenter()
                    .into(stepThumbnailImage);
        }
    }

    @Override
    public void showShortDescription(String shortDescription) {
        if (shortDescriptionTextView != null) {
            shortDescriptionTextView.setText(shortDescription);
        }
    }

    @Override
    public void clearShortDescription(String s) {
        if (shortDescriptionTextView != null) {
            shortDescriptionTextView.setText("");
        }
    }

    @Override
    public void clearDescription(String description) {
        if (descriptionTextView != null) {
            descriptionTextView.setText("");
        }
    }

    @Override
    public void showDescription(String description) {
        if (descriptionTextView != null) {
            descriptionTextView.setText(description);
        }
    }

    @Override
    public void playVideo(String videoURL, long currentVideoPosition) {
        simpleExoPlayerView.setVisibility(View.VISIBLE);

        TransferListener<? super DataSource> bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory =
                new AdaptiveTrackSelection.Factory((BandwidthMeter) bandwidthMeter);
        TrackSelector trackSelector =
                new DefaultTrackSelector(videoTrackSelectionFactory);

        if (player == null) {
            player = ExoPlayerFactory.newSimpleInstance(getContext(), trackSelector);
        }
        simpleExoPlayerView.setPlayer(player);

        DefaultExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();
        DefaultDataSourceFactory mediaDataSourceFactory = new DefaultDataSourceFactory(getContext(), Util.getUserAgent(getContext(), "mediaPlayerSample"), bandwidthMeter);

        MediaSource mediaSource = new ExtractorMediaSource(Uri.parse(videoURL),
                mediaDataSourceFactory, extractorsFactory, null, null);
        player.prepare(mediaSource);
        player.addListener(new ExoPlayer.EventListener() {
            @Override
            public void onTimelineChanged(Timeline timeline, Object manifest) {

            }

            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

            }

            @Override
            public void onLoadingChanged(boolean isLoading) {

            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                if (playbackState == STATE_READY) {
                    simpleExoPlayerView.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {

            }

            @Override
            public void onPositionDiscontinuity() {

            }

            @Override
            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

            }
        });

        if (currentVideoPosition != 0) {
            player.seekTo(currentVideoPosition);
        }

        player.setPlayWhenReady(true);
    }
}

