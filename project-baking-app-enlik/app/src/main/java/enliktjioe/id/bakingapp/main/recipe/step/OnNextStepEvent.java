package enliktjioe.id.bakingapp.main.recipe.step;

import enliktjioe.id.bakingapp.data.model.StepItem;

public class OnNextStepEvent {
    public StepItem stepItem;

    public OnNextStepEvent(StepItem stepItem) {
        this.stepItem = stepItem;
    }
}
