package enliktjioe.id.bakingapp.data.model;

import android.os.Parcelable;
import android.support.annotation.Nullable;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;

import java.util.List;

@AutoValue
public abstract class Recipe implements Parcelable {

    @Nullable
    @SerializedName("servings")
    abstract Integer servings();

    @Nullable
    @SerializedName("name")
    public abstract String name();

    @Nullable
    @SerializedName("image")
    public abstract String image();

    @Nullable
    @SerializedName("ingredients")
    public abstract List<IngredientsItem> ingredients();

    @Nullable
    @SerializedName("id")
    abstract Integer id();

    @Nullable
    @SerializedName("steps")
    public abstract List<StepItem> steps();

    public static TypeAdapter<Recipe> typeAdapter(Gson gson) {
        return new AutoValue_Recipe.GsonTypeAdapter(gson);
    }

    public static Recipe create(String name) {
        return new AutoValue_Recipe(null, name, null, null, null, null);
    }

    public static Recipe create(Integer servings, String name, String image, List<IngredientsItem> ingredients, Integer id, List<StepItem> steps) {
        return new AutoValue_Recipe(servings, name, image, ingredients, id, steps);
    }

}