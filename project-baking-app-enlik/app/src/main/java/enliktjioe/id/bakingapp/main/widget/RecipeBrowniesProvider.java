package enliktjioe.id.bakingapp.main.widget;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.widget.RemoteViews;

import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import enliktjioe.id.bakingapp.R;
import enliktjioe.id.bakingapp.data.RecipesCache;
import enliktjioe.id.bakingapp.data.model.IngredientsItem;
import enliktjioe.id.bakingapp.data.model.Recipe;
import enliktjioe.id.bakingapp.utilities.usecase.GetRecipesUseCase;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;

import static enliktjioe.id.bakingapp.BaseActivity.getGraph;

/**
 * Implementation of App Widget functionality.
 */
public class RecipeBrowniesProvider extends AppWidgetProvider {

    @Inject
    GetRecipesUseCase getRecipesUseCase;

    @Inject
    RecipesCache recipesCache;

    @Override
    public void onUpdate(Context context, final AppWidgetManager appWidgetManager, final int[] appWidgetIds) {
        if (recipesCache == null) {
            getGraph().inject(this);
        }

        if (recipesCache.isEmpty()) {
            getRecipesUseCase.execute(new Consumer<List<Recipe>>() {
                @Override
                public void accept(@NonNull List<Recipe> recipes) throws Exception {
                    recipesCache.clear();
                    recipesCache.addAll(recipes);
                    appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetIds, R.id.widget_content_brownies);
                }
            }, new Consumer<Throwable>() {
                @Override
                public void accept(@NonNull Throwable throwable) throws Exception {
                }
            });
        }
        for (int appWidgetId : appWidgetIds) {
            displayItems(context, appWidgetManager, appWidgetId);
        }

        super.onUpdate(context, appWidgetManager, appWidgetIds);
    }

    private void displayItems(Context context, AppWidgetManager appWidgetManager, int appWidgetId) {
        RemoteViews rv = new RemoteViews(context.getPackageName(), R.layout.widget_brownies);

        String textTitle, textIngredients;

        if (!recipesCache.isEmpty()) {
            textTitle = recipesCache.get(1).name();
            StringBuilder sb = new StringBuilder();
            for (IngredientsItem ingredientsItem : recipesCache.get(1).ingredients()) {
                if (sb.length() != 0) {
                    sb.append("\n");
                }

                sb.append(String.format(Locale.US, "%.1f %s %s", ingredientsItem.quantity(), ingredientsItem.measure(), ingredientsItem.ingredient()));
            }
            textIngredients = sb.toString();
        }

        else{
            textTitle = "Recipe load failed";
            textIngredients = "Please re-add Widget to home screen";
        }

        rv.setTextViewText(R.id.widget_title_brownies, textTitle);
        rv.setTextViewText(R.id.widget_content_brownies, textIngredients);

        appWidgetManager.updateAppWidget(appWidgetId, rv);

    }
}

