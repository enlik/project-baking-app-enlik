package enliktjioe.id.bakingapp.utilities.usecase;


import java.util.List;

import io.reactivex.Observable;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;
import enliktjioe.id.bakingapp.data.RecipesCache;
import enliktjioe.id.bakingapp.data.model.Recipe;
import enliktjioe.id.bakingapp.data.repo.recipes.RecipesRepository;
import enliktjioe.id.bakingapp.utilities.UseCase;
import enliktjioe.id.bakingapp.utilities.threading.PostExecutionThread;
import enliktjioe.id.bakingapp.utilities.threading.UseCaseExecutor;

public class GetRecipesUseCase extends UseCase<List<Recipe>> {

    private final RecipesRepository recipesRepository;
    private RecipesCache recipesCache;


    public GetRecipesUseCase(UseCaseExecutor useCaseExecutor, PostExecutionThread postExecutionThread, RecipesRepository recipesRepository, RecipesCache recipesCache) {
        super(useCaseExecutor, postExecutionThread);
        this.recipesRepository = recipesRepository;
        this.recipesCache = recipesCache;
    }


    @Override
    protected Observable<List<Recipe>> createObservable() {
        return recipesRepository.getAllRecipes().toObservable().map(new Function<List<Recipe>, List<Recipe>>() {
            @Override
            public List<Recipe> apply(@NonNull List<Recipe> recipes) throws Exception {
                if (recipes != null) {
                    if (recipesCache != null && recipesCache.isEmpty()) {
                        recipesCache.addAll(recipes);
                    }
                }
                return recipes;
            }
        });
    }
}
