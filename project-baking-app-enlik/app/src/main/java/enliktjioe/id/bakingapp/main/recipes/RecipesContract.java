package enliktjioe.id.bakingapp.main.recipes;


import java.util.List;

import enliktjioe.id.bakingapp.data.model.Recipe;

public interface RecipesContract {

    interface Presenter {
        void viewCreated();

        void retryButtonClicked();

        void bindView(View view);

        void paused();

        void onRecipeClick(Recipe recipe);
    }

    interface View {
        void hideRecipes();

        void hideError();

        void hideLoading();


        void showRecipe(Recipe recipe);

        void showLoading();

        void showRecipes(List<Recipe> recipes);

        void showError();
    }
}
