package enliktjioe.id.bakingapp;

import android.app.Application;

import javax.inject.Singleton;

import dagger.Component;
import enliktjioe.id.bakingapp.data.repo.recipes.RecipesRepositoryImpl;
import enliktjioe.id.bakingapp.main.widget.RecipeBrowniesProvider;
import enliktjioe.id.bakingapp.main.widget.RecipeCheesecakeProvider;
import enliktjioe.id.bakingapp.main.widget.RecipeNutellaPieProvider;
import enliktjioe.id.bakingapp.main.widget.RecipeYellowCakeProvider;
import enliktjioe.id.bakingapp.utilities.usecase.GetRecipesUseCase;
import enliktjioe.id.bakingapp.main.recipe.RecipeActivity;
import enliktjioe.id.bakingapp.main.recipe.step.RecipeStepFragment;
import enliktjioe.id.bakingapp.main.recipe.summary.RecipeSummaryFragment;
import enliktjioe.id.bakingapp.main.recipes.RecipesActivity;
import enliktjioe.id.bakingapp.main.widget.RecipeWidgetStackService;


@Singleton
@Component(modules = {
        BaseModule.class,
})
public interface BaseComponent {
    void inject(RecipesActivity recipesActivity);

    void inject(RecipesRepositoryImpl recipesRepository);

    void inject(GetRecipesUseCase getRecipesUseCase);

    void inject(RecipeActivity recipeActivity);

    void inject(RecipeSummaryFragment recipeSummaryFragment);

    void inject(RecipeStepFragment recipeStepFragment);

    void inject(RecipeWidgetStackService recipeWidgetStackService);

    void inject(RecipeNutellaPieProvider recipeNutellaPieProvider);

    void inject(RecipeBrowniesProvider recipeBrowniesProvider);

    void inject(RecipeYellowCakeProvider recipeYellowCakeProvider);

    void inject(RecipeCheesecakeProvider recipeCheesecakeProvider);


    final class Initializer {
        public static BaseComponent init(Application application) {
            return DaggerBaseComponent.builder()
                    .baseModule(new BaseModule(application))
                    .build();
        }
    }
}
