package enliktjioe.id.bakingapp.main.recipes;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import enliktjioe.id.bakingapp.data.model.Recipe;
import enliktjioe.id.bakingapp.main.recipes.items.RecipeItemView;
import enliktjioe.id.bakingapp.main.recipes.items.RecipeItemViewHolder;


class RecipesAdapter extends RecyclerView.Adapter<RecipeItemViewHolder> {
    private final List<Recipe> recipes;
    private final OnRecipeClick onRecipeClick;

    public RecipesAdapter(List<Recipe> recipes, OnRecipeClick onRecipeClick) {
        this.recipes = recipes;
        this.onRecipeClick = onRecipeClick;
    }


    @Override
    public RecipeItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RecipeItemViewHolder(new RecipeItemView(parent.getContext()), onRecipeClick);
    }

    @Override
    public void onBindViewHolder(final RecipeItemViewHolder holder, int position) {
        holder.getView().bind(recipes.get(position));
        holder.getView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.onRecipeClick.onRecipeClick(recipes.get(holder.getAdapterPosition()));
            }
        });

    }

    @Override
    public int getItemCount() {
        return recipes.size();
    }
}
