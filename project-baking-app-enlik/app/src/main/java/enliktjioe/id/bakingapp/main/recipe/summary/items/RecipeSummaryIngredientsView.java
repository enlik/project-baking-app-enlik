package enliktjioe.id.bakingapp.main.recipe.summary.items;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import enliktjioe.id.bakingapp.R;
import enliktjioe.id.bakingapp.data.model.IngredientsItem;


public class RecipeSummaryIngredientsView extends RelativeLayout {
    @BindView(R.id.summary_ingredients)
    TextView tv;


    public RecipeSummaryIngredientsView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public RecipeSummaryIngredientsView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RecipeSummaryIngredientsView(Context context) {
        this(context, null, 0);
    }


    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        ButterKnife.bind(this);
    }

    public void bind(List<IngredientsItem> ingredients) {
        StringBuilder sb = new StringBuilder();
        for (IngredientsItem ingredientsItem : ingredients) {
            if (sb.length() != 0) {
                sb.append("\n");
            }

            sb.append(String.format(Locale.US, "%.1f %s %s", ingredientsItem.quantity(), ingredientsItem.measure(), ingredientsItem.ingredient()));
        }
        tv.setText(sb.toString());
    }
}
