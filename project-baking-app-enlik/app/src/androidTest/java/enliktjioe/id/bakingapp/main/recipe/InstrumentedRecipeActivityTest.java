package enliktjioe.id.bakingapp.main.recipe;

import android.content.Context;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.Espresso;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.idling.CountingIdlingResource;
import android.support.test.espresso.intent.Intents;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.UiDevice;

import org.junit.After;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.Date;

import enliktjioe.id.bakingapp.BaseComponent;
import it.cosenonjaviste.daggermock.DaggerMockRule;
import enliktjioe.id.bakingapp.BaseActivity;
import enliktjioe.id.bakingapp.R;
import enliktjioe.id.bakingapp.data.model.IngredientsItem;
import enliktjioe.id.bakingapp.data.model.Recipe;
import enliktjioe.id.bakingapp.data.model.StepItem;
import enliktjioe.id.bakingapp.data.repo.recipes.RecipesRepository;
import enliktjioe.id.bakingapp.BaseModule;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class InstrumentedRecipeActivityTest {

    @Rule public DaggerMockRule<BaseComponent> daggerRule = new DaggerMockRule<>(BaseComponent.class, new BaseModule(getInstrumentation().getTargetContext().getApplicationContext()))
            .set(new DaggerMockRule.ComponentSetter<BaseComponent>() {
                @Override
                public void setComponent(BaseComponent component) {
                    BaseActivity app = (BaseActivity) getInstrumentation().getTargetContext().getApplicationContext();
                    app.setGraph(component);
                }
            });

    @Mock RecipesRepository repo;

    public Recipe recipe;

    @Rule
    public ActivityTestRule<RecipeActivity> mActivityTestRule = new ActivityTestRule<RecipeActivity>(RecipeActivity.class, false, false) {

        @Override
        protected Intent getActivityIntent() {
            Context targetContext = InstrumentationRegistry.getInstrumentation()
                    .getTargetContext();
            Intent result = new Intent(targetContext, RecipeActivity.class);

            ArrayList<IngredientsItem> ingredients = new ArrayList<>();
            ingredients.add(IngredientsItem.create(2.3f, "mg", "salt flowers"));
            ingredients.add(IngredientsItem.create(10f, "g", "chocolate"));

            ArrayList<StepItem> stepItems = new ArrayList<>();
            stepItems.add(StepItem.create("", "Pour the chocolate", 1, "pour chocolate", ""));
            stepItems.add(StepItem.create("", "Eat the great chocolate", 2, "eat great chocolate", ""));
            stepItems.add(StepItem.create("", "Redo the chocolate things", 3, "redo things", ""));

            recipe = Recipe.create(2, "cheesecake", "", ingredients, 42, stepItems);

            result.putExtra(RecipeActivity.EXTRA_RECIPE, recipe);
            return result;
        }
    };

    private CountingIdlingResource countingIdlingResource;
    private UiDevice uiDevice;


    @Before
    public void setUp() throws Exception {
        countingIdlingResource = new CountingIdlingResource("countingIdlingResource" + new Date().getTime());
        Espresso.registerIdlingResources(countingIdlingResource);

        uiDevice = UiDevice.getInstance(getInstrumentation());
        uiDevice.wakeUp();

        Intents.init();
    }

    @After
    public void tearDown() throws Exception {
        Espresso.unregisterIdlingResources(countingIdlingResource);

        Intents.release();
    }

    @Test
    public void testRecipeItemStepShowSinglePane() {

        Assume.assumeTrue(uiDevice.getDisplaySizeDp().x < 600);

        mActivityTestRule.launchActivity(null);


        onView(ViewMatchers.withId(R.id.fragment_summary_view))
                .perform(RecyclerViewActions.actionOnItemAtPosition(2, click()));

        onView(allOf(ViewMatchers.withText(recipe.steps().get(1).description())))
                .check(matches(isDisplayed()));
    }

    @Test
    public void testRecipeItemStepShowTwoPane() {

        Assume.assumeTrue(uiDevice.getDisplaySizeDp().x > 600);

        mActivityTestRule.launchActivity(null);


        onView(ViewMatchers.withId(R.id.fragment_summary_view))
                .perform(RecyclerViewActions.actionOnItemAtPosition(2, click()));


        onView(allOf(ViewMatchers.withText(recipe.steps().get(1).description())))
                .check(matches(isDisplayed()));

        onView(ViewMatchers.withId(R.id.fragment_summary_view))
                .perform(RecyclerViewActions.actionOnItemAtPosition(3, click()));

        onView(allOf(ViewMatchers.withText(recipe.steps().get(2).description())))
                .check(matches(isDisplayed()));

    }


}