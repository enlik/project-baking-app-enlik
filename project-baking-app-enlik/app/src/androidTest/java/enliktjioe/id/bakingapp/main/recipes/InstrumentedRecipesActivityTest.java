package enliktjioe.id.bakingapp.main.recipes;


import android.support.test.espresso.Espresso;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.idling.CountingIdlingResource;
import android.support.test.espresso.intent.Intents;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.UiDevice;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import enliktjioe.id.bakingapp.BaseComponent;
import enliktjioe.id.bakingapp.BaseModule;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;
import it.cosenonjaviste.daggermock.DaggerMockRule;
import enliktjioe.id.bakingapp.BaseActivity;
import enliktjioe.id.bakingapp.R;
import enliktjioe.id.bakingapp.data.model.Recipe;
import enliktjioe.id.bakingapp.data.repo.recipes.RecipesRepository;
import enliktjioe.id.bakingapp.data.repo.recipes.RecipesRepositoryImpl;
import enliktjioe.id.bakingapp.main.recipe.RecipeActivity;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static org.hamcrest.Matchers.not;
import static org.mockito.Mockito.doAnswer;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class InstrumentedRecipesActivityTest {

    private final static int DEFAULT_TIMEOUT = 500;

    @Rule public DaggerMockRule<BaseComponent> daggerRule = new DaggerMockRule<>(BaseComponent.class, new BaseModule(getInstrumentation().getTargetContext().getApplicationContext()))
            .set(new DaggerMockRule.ComponentSetter<BaseComponent>() {
                @Override
                public void setComponent(BaseComponent component) {
                    BaseActivity app = (BaseActivity) getInstrumentation().getTargetContext().getApplicationContext();
                    app.setGraph(component);
                }
            });

    @Mock RecipesRepository repo;

    @Rule
    public ActivityTestRule<RecipesActivity> mActivityTestRule = new ActivityTestRule<>(RecipesActivity.class, false, false);

    private CountingIdlingResource countingIdlingResource;


    @Before
    public void setUp() throws Exception {
        countingIdlingResource = new CountingIdlingResource("countingIdlingResource" + new Date().getTime());
        Espresso.registerIdlingResources(countingIdlingResource);

        UiDevice mDevice = UiDevice.getInstance(getInstrumentation());
        mDevice.wakeUp();

        Intents.init();
    }

    @After
    public void tearDown() throws Exception {
        Espresso.unregisterIdlingResources(countingIdlingResource);

        Intents.release();
    }

    @Test
    public void recipesFailingLoadingActivityTest_ShouldNotStartActivity() {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                return io.reactivex.Single.error(new Throwable("Intended Fail"));
            }
        }).when(repo).getAllRecipes();

        mActivityTestRule.launchActivity(null);


        onView(ViewMatchers.withId(R.id.recycler_view))
                .check(matches(not(isDisplayed())));

        intended(not(hasComponent(RecipeActivity.class.getName())));
    }

    @Test
    public void recipesNetworkLoadingActivityTest_ShouldStartActivity() {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                countingIdlingResource.increment();

                return new RecipesRepositoryImpl().getAllRecipes().map(new Function<List<Recipe>, List<Recipe>>() {
                    @Override
                    public List<Recipe> apply(@NonNull List<Recipe> recipes) throws Exception {
                        new Timer().schedule(new TimerTask() {
                            @Override
                            public void run() {
                                countingIdlingResource.decrement();
                            }
                        }, DEFAULT_TIMEOUT);

                        return recipes;
                    }
                });
            }
        }).when(repo).getAllRecipes();

        mActivityTestRule.launchActivity(null);


        onView(ViewMatchers.withId(R.id.recycler_view))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));

        intended(hasComponent(RecipeActivity.class.getName()));
    }
}
