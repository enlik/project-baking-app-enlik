
[![Build Status](https://travis-ci.org/mikklfr/BakingApp.svg?branch=master)](https://travis-ci.org/mikklfr/BakingApp)

#Baking App

This app allows to browse recipes and get step by step indications using text and videos.

## Screens
<img src="../master/captures/phone-baking-app-1.png" width="250">
<img src="../master/captures/phone-baking-app-2.png" width="250">
<img src="../master/captures/phone-baking-app-3.png" width="250">
<img src="../master/captures/tablet-baking-app-1.png" width="500">
<img src="../master/captures/tablet-baking-app-2.png" width="500">

## License

Copyright 2017 Michael Ohayon

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.